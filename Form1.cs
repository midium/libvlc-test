﻿using LibVLCSharp.Shared;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace LibVLCSharpTest
{
    public partial class Form1 : Form
    {
        protected LibVLC _libVLC;
        protected MediaPlayer _mp;


        public Form1()
        {
            InitializeComponent();

            Core.Initialize();

            _libVLC = new LibVLC();
            _libVLC.Log += _libVLC_Log;
        }


        private void _mp_LengthChanged(object sender, MediaPlayerLengthChangedEventArgs e)
        {
            if(this.InvokeRequired)
            {
                var method = new Action<object, MediaPlayerLengthChangedEventArgs>(_mp_LengthChanged);
                this.BeginInvoke(method, new object[] { this, e });
                return;
            }

            lblResult.Text = $"Media Type: {_mp.Media.Type}";
            lblResult.Text += Environment.NewLine + $"Tracks count: {_mp.Media.Tracks.Length}";
            foreach (var track in _mp.Media.Tracks)
                lblResult.Text += Environment.NewLine + $"Track Type: {track.TrackType}";
        }

        private void _libVLC_Log(object sender, LogEventArgs e)
        {
            if (this.InvokeRequired)
            {
                var method = new Action<object, LogEventArgs>(_libVLC_Log);
                this.BeginInvoke(method, new object[] { this, e });
                return;
            }
            Console.WriteLine(e.Message);
        }

        private void _mp_EncounteredError(object sender, EventArgs e)
        {
            if (this.InvokeRequired)
            {
                var method = new Action<object, EventArgs>(_mp_EncounteredError);
                this.BeginInvoke(method, new object[] { this, e });
                return;
            }

            Console.WriteLine($"{DateTime.UtcNow:o} ENCOUNTERED ERROR");
        }

        private void button1_Click(object sender, EventArgs e)
        {
            using (var diag = new OpenFileDialog())
            {

                if (diag.ShowDialog() != DialogResult.OK) return;

                _mp = new MediaPlayer(_libVLC);
                _mp.EnableKeyInput = false;
                _mp.EnableMouseInput = false;
                _mp.EncounteredError += _mp_EncounteredError;

                _mp.LengthChanged += _mp_LengthChanged;

                var media = new Media(_libVLC, diag.FileName, FromType.FromPath);

                _mp.Play(media);


            }
        }
    }
}
